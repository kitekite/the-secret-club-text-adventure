extends Control

const Scene = preload("res://script/model/Scene.gd")

onready var story_panel = get_node("BackgroundPanel/MarginContainer/HBoxContainer/StoryPanel")
onready var options_panel = get_node("BackgroundPanel/MarginContainer/HBoxContainer/OptionsPanel")


func set_scene(scene: Scene, receiver=null, method=null) -> void:
	story_panel.set_text(scene.story_text)
	options_panel.set_options(scene.options, receiver, method)
