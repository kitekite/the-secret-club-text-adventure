extends PanelContainer

const OptionsButton = preload("res://scene/OptionsButton.tscn")
onready var buttons_container = get_node("ScrollContainer/MarginContainer/ButtonsContainer")


func set_options(options: Array, receiver=null, method=null):
	print_debug("optoins: ", options)

	self._clear_options()

	for option in options:
		var button = OptionsButton.instance()
		button.text = option["label"]
		if (receiver != null):
			button.connect("button_up", receiver, method, [option["next"]])
		buttons_container.add_child(button)


func _clear_options():
	for node in buttons_container.get_children():
		buttons_container.remove_child(node)
		node.queue_free()
