extends PanelContainer

onready var text_label = get_node("ScrollContainer/MarginContainer/TextLabel")


func set_text(text: String):
	text_label.text = text
