extends Object


### Properties

var id: int setget , id_get
var story_text: String setget , story_text_get
var options: Array setget , options_get  # Type = [{label: String, next: int}]


### Getter and Setter

func id_get() -> int:
	return id


func story_text_get() -> String:
	return story_text


func options_get() -> Array:
	return options


### Methods

func _init(p_id: int, p_story_text: String, p_options: Array):
	id = p_id
	story_text = p_story_text
	options = p_options

