extends Object

const Scene = preload("res://script/model/Scene.gd")


func _load_file(filePath: String) -> String:
	var file = File.new()
	file.open(filePath, File.READ)
	var content = file.get_as_text()
	file.close()
	return content


func _json_to_scene_dict(json: JSONParseResult) -> Dictionary:
	var result = {}

	# Load "metadata"
	result["metadata"] = json.result["metadata"]

	# Load "options"
	result["options"] = json.result["options"]

	# Load "scenes"
	result["scenes"] = {}
	for scene in json.result["scenes"]:
		var id = int(scene["id"])
		var story_text = scene["story_text"]
		var scene_options = []
		for scene_option in scene["options"]:
			scene_options.append(
				{"label": scene_option["label"], "next": scene_option["next"]}
			)

		var scene_obj: Scene = Scene.new(id, story_text, scene_options)
		result["scenes"][id] = scene_obj
	
	return result


func load_scene_db(res: String) -> Dictionary:
	var db_text = _load_file(res)
	var json_result = JSON.parse(db_text)

	if json_result.error != OK:
		printerr("SceneDB loading failed. Error: ", json_result.error)
		return {}
	
	return self._json_to_scene_dict(json_result)
