extends Node

const Scene = preload("res://script/model/Scene.gd")
const SceneDBLoader = preload("res://script/controller/SceneDBLoader.gd")

onready var ui = get_node("../MainUI")

var scene_db: Dictionary


func _ready() -> void:
	ui.connect("ready", self, "_on_MainUI_ready")
	var scene_db_loader = SceneDBLoader.new()
	# TODO: better loader API
	scene_db = scene_db_loader.load_scene_db("res://data/scenedb.json")

	print_debug("SceneController ready.")


func _on_MainUI_ready() -> void:
	var first_scene_id = scene_db["options"]["first_scene"]
	var first_scene = self._get_scene_by_id(first_scene_id)
	ui.set_scene(first_scene, self, "_on_options_button_pressed")


### Signal Receivers

func _on_options_button_pressed(next: int):
	print_debug("button pressed. next: ", next)
	var next_scene = self._get_scene_by_id(next)
	ui.set_scene(next_scene, self, "_on_options_button_pressed")


### Scene DB Accessors

func _get_scene_by_id(id: int) -> Scene:
	return scene_db["scenes"][id]
